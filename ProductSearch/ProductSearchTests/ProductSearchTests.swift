//
//  ProductSearchTests.swift
//  ProductSearchTests
//
//  Created by Mariano Manuel on 4/13/21.
//

import XCTest
@testable import ProductSearch

class ProductSearchTests: XCTestCase {
    var session: URLSession!
    let searchURL = URL(string: "https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi/search?&query=Nintendo")

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        session = URLSession(configuration: .default)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        session = nil
        super.tearDown()
    }
    
    func testValidHTTPStatusCode200() {
        let promise = expectation(description: "Status Code: 200")
        var request = URLRequest(url: searchURL!)
        request.httpMethod = "GET"
        request.setValue("adb8204d-d574-4394-8c1a-53226a40876e", forHTTPHeaderField: "X-IBM-Client-Id")
        
        let dataTask = session.dataTask(with: request) { data, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        wait(for: [promise], timeout: 5)
    }
    
    func testAPICallCompletes() {
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        var request = URLRequest(url: searchURL!)
        request.httpMethod = "GET"
        request.setValue("adb8204d-d574-4394-8c1a-53226a40876e", forHTTPHeaderField: "X-IBM-Client-Id")
        
        let dataTask = session.dataTask(with: request) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        dataTask.resume()
        wait(for: [promise], timeout: 5)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

}
