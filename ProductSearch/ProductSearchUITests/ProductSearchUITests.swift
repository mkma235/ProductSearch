//
//  ProductSearchUITests.swift
//  ProductSearchUITests
//
//  Created by Mariano Manuel on 4/13/21.
//

import XCTest

class ProductSearchUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        app = XCUIApplication()
        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchBarLayout() {
        let searchBar = app.textFields.element
        XCTAssert(searchBar.exists)
        XCTAssert(searchBar.isHittable)
        searchBar.tap()
        
        let cancel = app.buttons["Cancel"]
        XCTAssert(cancel.exists)
        XCTAssert(cancel.isHittable)
        cancel.tap()
        searchBar.tap()
        searchBar.typeText("Mario")
        app.keyboards.buttons["return"].tap()
        
        let loadingScreen = app.activityIndicators.element
        
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (_) in
            XCTAssert(loadingScreen.exists)
            XCTAssert(loadingScreen.isHittable)
        }
        
    }
    
    func testProductListingLayout() {
        let searchBar = app.textFields.element
        searchBar.tap()
        searchBar.typeText("Nintendo")
        app.keyboards.buttons["return"].tap()
        
        let expectation = XCTestExpectation(description: "Load Products")
        
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (_) in
            let scrollView = self.app.scrollViews.element
            XCTAssert(scrollView.exists)
            XCTAssert(scrollView.isHittable)
            self.app.swipeUp()
            self.app.swipeUp()
            self.app.swipeDown()
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 20.0)
        
        let expectation2 = XCTestExpectation(description: "Check Search History")
        
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (_) in
            searchBar.tap()
            let list = self.app.tables.element
            XCTAssert(list.exists)
            XCTAssert(list.isHittable)
            list.cells.buttons["Nintendo"].swipeLeft()
            list.cells.buttons["Delete"].tap()
            expectation2.fulfill()
        }
        wait(for: [expectation2], timeout: 20.0)
    }

}
