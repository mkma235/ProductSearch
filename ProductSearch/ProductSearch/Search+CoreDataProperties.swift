//
//  Search+CoreDataProperties.swift
//  ProductSearch
//
//  Created by Mariano Manuel on 4/14/21.
//
//

import Foundation
import CoreData


extension Search {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Search> {
        return NSFetchRequest<Search>(entityName: "Search")
    }

    @NSManaged public var id: UUID
    @NSManaged public var text: String

}

extension Search : Identifiable {

}
