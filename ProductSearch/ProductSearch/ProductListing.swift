//
//  ProductListing.swift
//  ProductSearch
//
//  Created by Mariano Manuel on 4/13/21.
//

import SwiftUI
import CoreData

struct ProductListing: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(entity: Search.entity(), sortDescriptors: []) private var searchHistory: FetchedResults<Search>
    @State private var searchText: String = ""
    @State private var isEditing: Bool = false
    @State private var loadingData: Bool = false
    @State private var linear: Bool = true
    @State private var progress: Double = 0.0
    @State var searchService: SearchService = SearchService()
    @State var results: [Any] = []
    @State var imageData: [Data] = []
    @State var titleData: [String] = []
    @State var priceData: [String] = []
    private var gridLayout = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        VStack {
            SearchView(text: $searchText, editing: $isEditing, results: $results, imageData: $imageData, titleData: $titleData, priceData: $priceData, loadingData: $loadingData, linear: $linear, progress: $progress).environment(\.managedObjectContext, viewContext)
                .padding(8)
            if isEditing {
                List() {
                    Section(header: Text("History")) {
                        ForEach(searchHistory.filter({ $0.text.contains(searchText) || searchText.isEmpty }), id: \.id) { (search) in
                            Button(action: {
                                isEditing.toggle()
                                // dismiss keyboard
                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                // perform past search
                                searchService.fetchSearchResults(searchTerms: search.text) { (jsonArray) in
                                    if linear {
                                        loadingData = true
                                    }
                                    if let jsonArray = jsonArray {
                                        results = jsonArray
                                    }
                                    
                                    DispatchQueue.global().async {
                                        var images: [Data] = []
                                        var titles: [String] = []
                                        var prices: [String] = []
                                        let increment: Double = 100.0 / Double(results.count)
                                        for i in 0..<results.count {
                                            progress += increment
                                            let product = results[i] as! [String: Any]
                                            let strURL = product["image"] as! String
                                            let imageURL = URL(string: strURL)!
                                            do {
                                                let data = try Data(contentsOf: imageURL)
                                                images.append(data)
                                            } catch  {
                                                print(error)
                                                exit(-1)
                                            }
                                            let title = product["title"] as! String
                                            titles.append(title)
                                            let price = product["price"] as! Double
                                            let formatter = NumberFormatter()
                                            formatter.numberStyle = .currency
                                            let amount = formatter.string(from: NSNumber(value: price))
                                            prices.append(amount!)
                                        }
                                        DispatchQueue.main.sync {
                                            loadingData = true
                                            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (_) in
                                                imageData = images
                                                titleData = titles
                                                priceData = prices
                                                loadingData = false
                                                linear = false
                                            }
                                        }
                                    }
                                }
                            }, label: {
                                Text(search.text)
                            })
                        }
                        .onDelete(perform: { indexSet in
                            indexSet.forEach() { index in
                                viewContext.delete(searchHistory[index])
                            }
                            do {
                                try viewContext.save()
                            } catch {
                                print(error.localizedDescription)
                            }
                        })
                    }
                    .textCase(nil)
                }
            } else if loadingData {
                Spacer()
                if linear {
                    ProgressView(value: progress, total: 100.0)
                        .progressViewStyle(LinearProgressViewStyle())
                        .padding()
                } else {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                        .padding()
                }
            } else {
                ScrollView {
                    LazyVGrid(columns: gridLayout, spacing: 20) {
                        ForEach(0..<priceData.count, id: \.self) { (index) in
                            HStack {
                                VStack {
                                    Image(uiImage: UIImage(data: imageData[index])!)
                                        .font(.system(size: 30))
                                        .frame(width: 100, height: 100)
                                        .cornerRadius(20)
                                    Text(titleData[index])
                                        .lineLimit(3)
                                    Text(priceData[index])
                                }
                            }
                        }
                    }
                    .padding()
                }
            }
            Spacer()
        }
    }
}

struct ProductListing_Previews: PreviewProvider {
    static var previews: some View {
        ProductListing()
    }
}
