//
//  SearchView.swift
//  ProductSearch
//
//  Created by Mariano Manuel on 4/13/21.
//

import SwiftUI

struct SearchView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(entity: Search.entity(), sortDescriptors: []) private var searchHistory: FetchedResults<Search>
    @State var searchService: SearchService = SearchService()
    @State var repeatedSearch: Bool = false
    @Binding var text: String
    @Binding var editing: Bool
    @Binding var results: [Any]
    @Binding var imageData: [Data]
    @Binding var titleData: [String]
    @Binding var priceData: [String]
    @Binding var loadingData: Bool
    @Binding var linear: Bool
    @Binding var progress: Double
    
    var body: some View {
        HStack {
            TextField("Search", text: $text, onCommit: {
                editing.toggle()
                repeatedSearch = false
                //Create Past Search
                searchHistory.forEach() { search in
                    if search.text == text {
                        repeatedSearch = true
                    }
                }
                if !repeatedSearch {
                    let query = Search(context: viewContext)
                    query.text = text
                    query.id = UUID()
                    do {
                        try viewContext.save()
                    } catch {
                        print(error.localizedDescription)
                    }
                }
                // perform search
                searchService.fetchSearchResults(searchTerms: text) { (jsonArray) in
                    if linear {
                        loadingData = true
                    }
                    if let jsonArray = jsonArray {
                        results = jsonArray
                    }
                    DispatchQueue.global().async {
                        var images: [Data] = []
                        var titles: [String] = []
                        var prices: [String] = []
                        let increment: Double = 100.0 / Double(results.count)
                        for i in 0..<results.count {
                            progress += increment
                            let product = results[i] as! [String: Any]
                            let strURL = product["image"] as! String
                            let imageURL = URL(string: strURL)!
                            do {
                                let data = try Data(contentsOf: imageURL)
                                images.append(data)
                            } catch  {
                                print(error)
                                exit(-1)
                            }
                            let title = product["title"] as! String
                            titles.append(title)
                            let price = product["price"] as! Double
                            let formatter = NumberFormatter()
                            formatter.numberStyle = .currency
                            let amount = formatter.string(from: NSNumber(value: price))
                            prices.append(amount!)
                        }
                        DispatchQueue.main.sync {
                            loadingData = true
                            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (_) in
                                imageData = images
                                titleData = titles
                                priceData = prices
                                loadingData = false
                                linear = false
                            }
                        }
                    }
                }
                text = ""
            })
                .padding(8)
                .padding(.horizontal, 25)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                        if editing {
                            Button(action: {
                                self.text = ""
                            }, label: {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.gray)
                                    .padding(.trailing, 8)
                            })
                        }
                    }
                )
                .padding(.horizontal, 10)
                .onTapGesture {
                    self.editing = true
                }
            
            if editing {
                Button(action: {
                    self.editing.toggle()
                    self.text = ""
                    // dismiss keyboard
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                }, label: {
                    Text("Cancel")
                })
                .padding(.trailing, 10)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(text: .constant(""), editing: .constant(false), results: .constant(["Results"]), imageData: .constant([Data.init(count: 0)]), titleData: .constant(["Titles"]), priceData: .constant(["Prices"]), loadingData: .constant(false), linear: .constant(false), progress: .constant(0.0))
    }
}
