//
//  ProductSearchApp.swift
//  ProductSearch
//
//  Created by Mariano Manuel on 4/13/21.
//

import SwiftUI

@main
struct ProductSearchApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ProductListing().environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
