//
//  SearchService.swift
//  ProductSearch
//
//  Created by Mariano Manuel on 4/14/21.
//

import Foundation


class SearchService {
    var searchURL = "https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi/search?&query="
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
                
    func fetchSearchResults(searchTerms: String, completion: @escaping ([Any]?) -> Void) {
        dataTask?.cancel()
        let query = searchTerms.replacingOccurrences(of: " ", with: "+")
        let queryURL = searchURL + query
        guard let url = URL(string: queryURL) else { return }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("adb8204d-d574-4394-8c1a-53226a40876e", forHTTPHeaderField: "X-IBM-Client-Id")
                
        dataTask = defaultSession.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
            } else if let data = data {
                var response: [String: Any]?
                do {
                    response = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch _ as NSError { return }
                guard let array = response!["items"] as? [Any] else { return }
                DispatchQueue.main.async {
                    completion(array)
                }
            }
        }
        dataTask?.resume()
    }
}
